package app.carrepair;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import app.carrepair.Models.Booking;
import app.carrepair.Models.Merchant;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PendingServiceActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    String isMerchant;

    ArrayList<Booking> bookings;

    @BindView(R.id.lv_bookings)
    ListView lvBookings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_service);
        ButterKnife.bind(this);
        showProgressDialog();

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        bookings = new ArrayList<>();
        getAllBookings();

        Intent intent = getIntent();
        isMerchant = intent.getStringExtra("isMerchant");

        if(isMerchant == null)
        {
            isMerchant = "false";
        }

        lvBookings.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {
                Booking booking = (Booking) lvBookings.getItemAtPosition(position);

                if(isMerchant.equals("true"))
                {
                    Intent intent = new Intent(getApplicationContext(),PendingServiceMerchantDetailActivity.class);
                    intent.putExtra("id", booking.getFirebaseKey());
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getApplicationContext(),PendingServiceDetailActivity.class);
                    intent.putExtra("id", booking.getFirebaseKey());
                    startActivity(intent);
                }
            }
        });
    }

    public void setAdapter(){
        BookingAdapter adapter =
                new BookingAdapter(this, bookings);
        lvBookings.setAdapter(adapter);
        hideProgressDialog();
    }

    public void getAllBookings(){
        mDatabase.child("Bookings").addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot child: dataSnapshot.getChildren())
                        {
                            Booking booking = child.getValue(Booking.class);
                            booking.setFirebaseKey(child.getKey());

                            findViewById(R.id.empty_message).setVisibility(View.GONE);

                            if(isMerchant.equals("true"))
                            {
                                if(booking.getMerchantId().equals(user.getUid()))
                                {
                                    bookings.add(booking);
                                }
                            }else{
                                if(booking.getUserId().equals(user.getUid()))
                                {
                                    bookings.add(booking);
                                }
                            }

                        }
                        setAdapter();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Firebase:", "error", databaseError.toException());
                    }
                });
    }
}