package app.carrepair.Models;

import android.media.Image;

import java.util.ArrayList;

public class Merchant {
    private String CompanyName;
    private String PersonInChargeName;
    private Integer Phone;
    private String CompanyAddress;
    private String ImageUrl;
    private Double Longitude;
    private Double Latitude;
    private String FirebaseKey;
    private int Distance;

    public Merchant(){}

    public Merchant(String companyName, String personInChargeName, Integer phone, String companyAddress, Double longitude, Double latitude, String imageUrl) {
        CompanyName = companyName;
        PersonInChargeName = personInChargeName;
        Phone = phone;
        CompanyAddress = companyAddress;
        Longitude = longitude;
        Latitude = latitude;
        ImageUrl = imageUrl;
    }

    public int getDistance() {
        return Distance;
    }

    public void setDistance(int distance) {
        Distance = distance;
    }

    public Merchant(String companyName, String personInChargeName, Integer phone, String companyAddress) {
        CompanyName = companyName;
        PersonInChargeName = personInChargeName;
        Phone = phone;
        CompanyAddress = companyAddress;
    }

    public String getFirebaseKey() {
        return FirebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        FirebaseKey = firebaseKey;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getPersonInChargeName() {
        return PersonInChargeName;
    }

    public void setPersonInChargeName(String personInChargeName) {
        PersonInChargeName = personInChargeName;
    }

    public Integer getPhone() {
        return Phone;
    }

    public void setPhone(Integer phone) {
        Phone = phone;
    }

    public String getCompanyAddress() {
        return CompanyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        CompanyAddress = companyAddress;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }
}
