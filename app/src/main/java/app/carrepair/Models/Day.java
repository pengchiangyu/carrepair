package app.carrepair.Models;

public class Day {
    private Boolean isOn;
    private Time openTime;
    private Time closeTime;

    public Boolean getOn() {
        return isOn;
    }

    public void setOn(Boolean on) {
        isOn = on;
    }

    public Time getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Time openTime) {
        this.openTime = openTime;
    }

    public Time getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Time closeTime) {
        this.closeTime = closeTime;
    }

    public String getOperationHour(){
        return this.openTime.getFormattedTime() + " - " + this.closeTime.getFormattedTime();
    }

    public Day(Boolean isOn, Time openTime, Time closeTime) {

        this.isOn = isOn;
        this.openTime = openTime;
        this.closeTime = closeTime;
    }

    public Day(){
        this.isOn = false;
        this.openTime = new Time();
        this.openTime.setHour(9);
        this.openTime.setMinute(0);
        this.closeTime = new Time();
        this.closeTime.setHour(18);
        this.closeTime.setMinute(0);
    }
}
