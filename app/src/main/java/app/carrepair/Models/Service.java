package app.carrepair.Models;

public class Service {
    private boolean IsAvailable;
    private String Name;

    public boolean isAvailable() {
        return IsAvailable;
    }

    public void setAvailable(boolean available) {
        IsAvailable = available;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Service(boolean isAvailable, String name) {
        IsAvailable = isAvailable;
        Name = name;

    }

    public Service(){}
}
