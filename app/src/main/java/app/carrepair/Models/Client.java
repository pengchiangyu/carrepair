package app.carrepair.Models;

public class Client {
    private String Name;
    private Integer Phone;
    private String CarBrand;
    private String CarModel;

    public Client(String name, Integer phone, String carBrand, String carModel) {
        Name = name;
        Phone = phone;
        CarBrand = carBrand;
        CarModel = carModel;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getPhone() {
        return Phone;
    }

    public void setPhone(Integer phone) {
        Phone = phone;
    }

    public String getCarBrand() {
        return CarBrand;
    }

    public void setCarBrand(String carBrand) {
        CarBrand = carBrand;
    }

    public String getCarModel() {
        return CarModel;
    }

    public void setCarModel(String carModel) {
        CarModel = carModel;
    }

    public Client(){}
}
