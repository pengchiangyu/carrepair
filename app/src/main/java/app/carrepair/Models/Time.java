package app.carrepair.Models;

public class Time {
    private int hour;
    private int minute;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public Time(int hour, int minute) {

        this.hour = hour;
        this.minute = minute;
    }

    public String getFormattedTime(){
        return String.format("%02d", this.hour) + ":" + String.format("%02d", this.minute);
    }

    public Time(){}
}
