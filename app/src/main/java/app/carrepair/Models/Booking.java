package app.carrepair.Models;

public class Booking {
    private String Description;
    private String Date;
    private String Time;
    private String UserId;
    private String MerchantId;
    private String Status;

    public String getFirebaseKey() {
        return FirebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        FirebaseKey = firebaseKey;
    }

    private String FirebaseKey;

    public Booking(String description, String date, String time, String userId, String merchantId, String status) {
        Description = description;
        Date = date;
        Time = time;
        UserId = userId;
        MerchantId = merchantId;
        Status = status;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDescription() {

        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(String merchantId) {
        MerchantId = merchantId;
    }

    public String getDateTime(){
        return Date + " - " + Time;
    }

    public Booking(){}
}
