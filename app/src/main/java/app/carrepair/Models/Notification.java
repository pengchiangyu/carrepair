package app.carrepair.Models;

public class Notification {
    private String DateTime;
    private String Message;

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Notification(String dateTime, String message) {

        DateTime = dateTime;
        Message = message;
    }

    public Notification(){}
}
