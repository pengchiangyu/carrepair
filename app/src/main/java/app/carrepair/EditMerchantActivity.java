package app.carrepair;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import app.carrepair.Models.Merchant;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditMerchantActivity extends BaseActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private GoogleSignInClient mGoogleSignInClient;
    private DatabaseReference mDatabase;

    private static final int REQUEST_IMAGE_CAPTURE = 111;
    private static final int REQUEST_IMAGE_GALLERY = 222;
    private static final int PLACE_PICKER_REQUEST = 333;

    private Place place;
    Double latitude;
    Double longitude;

    private String imageUrl;

    Merchant merchant;

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.companyName)
    EditText companyName;

    @BindView(R.id.pic)
    EditText pic;

    @BindView(R.id.phone)
    EditText phone;

    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.selected_location)
    TextView selectedLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_merchant);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mDatabase.child("Merchants").child(firebaseUser.getUid()).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        merchant = dataSnapshot.getValue(Merchant.class);

                        if(merchant == null)
                        {
                            return;
                        }

                        if(merchant.getImageUrl() != null)
                        {
                            image.setVisibility(View.VISIBLE);
                            byte[] decodedString = Base64.decode(merchant.getImageUrl(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            image.setImageBitmap(decodedByte);

                            imageUrl = merchant.getImageUrl();
                        }
                        if(merchant.getLatitude() != null && merchant.getLongitude() != null)
                        {
                            selectedLocation.setText("Latitude: " + merchant.getLatitude() + ", Longitude: " + merchant.getLongitude());
                            selectedLocation.setVisibility(View.VISIBLE);

                            latitude = merchant.getLatitude();
                            longitude = merchant.getLongitude();
                        }

                        companyName.setText(merchant.getCompanyName());
                        pic.setText(merchant.getPersonInChargeName());
                        phone.setText(String.format("%d",merchant.getPhone()));
                        address.setText(merchant.getCompanyAddress());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Firebase:", "getUser:onCancelled", databaseError.toException());
                    }
                });
    }

    @OnClick(R.id.save)
    public void onSaveButtonClicked(){
        showProgressDialog();

        if(!tryParseInt(phone.getText().toString()))
        {
            hideProgressDialog();
            Toast.makeText(EditMerchantActivity.this, "Input only number at phone field!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if(!validateFields(new EditText[]{phone, companyName, pic, address })){
            hideProgressDialog();
            Toast.makeText(EditMerchantActivity.this, "All the fields cannot be empty!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if((latitude == null && longitude == null))
        {
            hideProgressDialog();
            Toast.makeText(EditMerchantActivity.this, "Please select a location first!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        Merchant newMerchant = new Merchant(
                companyName.getText().toString(),
                pic.getText().toString(),
                Integer.parseInt(phone.getText().toString()),
                address.getText().toString());

        if(imageUrl != null)
        {
            newMerchant.setImageUrl(imageUrl);
        }

        if(place != null)
        {
            newMerchant.setLatitude(latitude);
            newMerchant.setLongitude(longitude);
        }

        mDatabase.child("Users").child(firebaseUser.getUid()).child("Details").setValue(newMerchant);
        Toast.makeText(EditMerchantActivity.this, "Saved!",
                Toast.LENGTH_LONG).show();
        finish();
    }

    private boolean validateFields(EditText[] fields){
        for (EditText currentField : fields) {
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


    @OnClick(R.id.location_button)
    public void onSelectLocation(){
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            showProgressDialog();
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.image)
    public void onImageButtonClicked(){
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(EditMerchantActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);//
                    startActivityForResult(Intent.createChooser(intent, "Select File"),REQUEST_IMAGE_GALLERY);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void encodeBitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        String imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        imageUrl = imageEncoded;
        image.setImageBitmap(bitmap);
    }

    @OnClick(R.id.operation_hour_or_service)
    public void onEditOperationHourClicked(){
        Intent intent = new Intent(getApplicationContext(),ChooseOperationHourActivity.class);
        intent.putExtra("isEditing", "true");
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            if(imageBitmap == null)
            {
                return;
            }
            encodeBitmap(imageBitmap);
        }
        else if (requestCode == REQUEST_IMAGE_GALLERY) {
            Bitmap bitmap;
            try {
                if(data == null)
                {
                    return;
                }
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                encodeBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                place = PlacePicker.getPlace(data, this);
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;

                selectedLocation.setText("Latitude: " + latitude + ", Longitude: " + longitude);
                selectedLocation.setVisibility(View.VISIBLE);

                hideProgressDialog();
            }
            hideProgressDialog();
        }
        else{
            Log.w("Activity Result:", "Error");
        }
    }
}
