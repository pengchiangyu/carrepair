package app.carrepair;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import app.carrepair.Models.Merchant;
import app.carrepair.helpers.PasswordHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpMerchantActivity extends BaseActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    private String imageUrl;
    private static final int REQUEST_IMAGE_CAPTURE = 111;
    private static final int REQUEST_IMAGE_GALLERY = 222;
    private static final int PLACE_PICKER_REQUEST = 333;

    private Place place;
    double latitude;
    double longitude;

    protected GeoDataClient geoDataClient;
    protected PlaceDetectionClient placeDetectionClient;

    @BindView(R.id.next)
    Button nextButton;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.companyName)
    EditText companyName;

    @BindView(R.id.pic)
    EditText pic;

    @BindView(R.id.phone)
    EditText phone;

    @BindView(R.id.address)
    EditText address;

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.selected_location)
    TextView selectedLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_merchant);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        password.setTransformationMethod(new PasswordHelper());

        geoDataClient = Places.getGeoDataClient(this, null);
        placeDetectionClient = Places.getPlaceDetectionClient(this, null);

        place = null;

        latitude = 0;
        longitude = 0;
    }

    @OnClick(R.id.location_button)
    public void onSelectLocation(){
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            showProgressDialog();
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.image)
    public void onImageButtonClicked(){
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpMerchantActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);//
                    startActivityForResult(Intent.createChooser(intent, "Select File"),REQUEST_IMAGE_GALLERY);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @OnClick(R.id.next)
    public void onSignUpButtonClicked(){
        showProgressDialog();

        if(!tryParseInt(phone.getText().toString()))
        {
            hideProgressDialog();
            Toast.makeText(SignUpMerchantActivity.this, "Input only number at phone field!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if(!validateFields(new EditText[]{email, password, phone, companyName, pic, address })){
            hideProgressDialog();
            Toast.makeText(SignUpMerchantActivity.this, "All the fields cannot be empty!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if(place == null)
        {
            hideProgressDialog();
            Toast.makeText(SignUpMerchantActivity.this, "Please select a location first!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        String email = this.email.getText().toString();
        String password = this.password.getText().toString();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            CreateUserDatabase();
                            hideProgressDialog();
                            startActivity(new Intent(getApplicationContext(),ChooseOperationHourActivity.class));
                        } else {
                            // If sign in fails, display a message to the user.
                            hideProgressDialog();
                            Log.w("Firebase: ", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUpMerchantActivity.this, task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    private boolean validateFields(EditText[] fields){
        for (EditText currentField : fields) {
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void CreateUserDatabase(){
        user = mAuth.getCurrentUser();

        Merchant merchant = new Merchant(
                companyName.getText().toString(),
                pic.getText().toString(),
                Integer.parseInt(phone.getText().toString()),
                address.getText().toString());

        if(imageUrl != null)
        {
            merchant.setImageUrl(imageUrl);
        }

        if(place != null)
        {
            merchant.setLatitude(latitude);
            merchant.setLongitude(longitude);
        }

        mDatabase.child("Merchants").child(user.getUid()).setValue(merchant);
        mDatabase.child("Users").child(user.getUid()).child("Category").setValue("merchant");
    }

    public void encodeBitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        String imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        imageUrl = imageEncoded;
        image.setImageBitmap(bitmap);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            if(imageBitmap == null)
            {
                return;
            }
            encodeBitmap(imageBitmap);
        }
        else if (requestCode == REQUEST_IMAGE_GALLERY) {
            Bitmap bitmap;
            try {
                if(data == null)
                {
                    return;
                }
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                encodeBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                place = PlacePicker.getPlace(data, this);
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;

                selectedLocation.setText("Latitude: " + latitude + ", Longitude: " + longitude);
                selectedLocation.setVisibility(View.VISIBLE);

                hideProgressDialog();
            }
            hideProgressDialog();
        }
        else{
            Log.w("Activity Result:", "Error");
        }
    }
}
