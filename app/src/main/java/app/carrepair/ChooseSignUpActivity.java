package app.carrepair;

import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseSignUpActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_signup);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.client_button)
    public void onClientButtonClicked(){
        startActivity(new Intent(getApplicationContext(),SignUpClientActivity.class));
    }

    @OnClick(R.id.merchant_button)
    public void onMerchantButtonClicked(){
        startActivity(new Intent(getApplicationContext(),SignUpMerchantActivity.class));
    }
}
