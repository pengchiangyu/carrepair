package app.carrepair;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import app.carrepair.Models.Day;
import app.carrepair.Models.Days;
import app.carrepair.Models.Service;
import app.carrepair.Models.Time;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseServiceActivity extends BaseActivity {

    @BindView(R.id.engine)
    CheckBox engine;

    @BindView(R.id.lighting)
    CheckBox lighting;

    @BindView(R.id.battery)
    CheckBox battery;

    @BindView(R.id.aircond)
    CheckBox aircond;

    @BindView(R.id.brake)
    CheckBox brake;

    @BindView(R.id.windscreen)
    CheckBox windscreen;

    @BindView(R.id.interior)
    CheckBox interior;

    @BindView(R.id.electrical)
    CheckBox electrical;

    @BindView(R.id.painting)
    CheckBox painting;

    @BindView(R.id.accessory)
    CheckBox accessory;

    @BindView(R.id.tire)
    CheckBox tire;

    @BindView(R.id.other)
    CheckBox other;

    @BindView(R.id.other_text)
    EditText otherText;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    String isEditing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        Intent intent = getIntent();
        isEditing = intent.getStringExtra("isEditing");
        if(isEditing == null)
        {
            isEditing  = "false";
        }
    }

    @OnClick(R.id.other)
    public void onOtherclicked(){
        if(other.isChecked())
        {
            findViewById(R.id.other_text).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.other_text).setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.finish)
    public void onFinishButtonClicked(){
        ArrayList<Service> services = new ArrayList<>();

        if(engine.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(engine.getText().toString());
            services.add(service);
        }

        if(lighting.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(lighting.getText().toString());
            services.add(service);
        }

        if(battery.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(battery.getText().toString());
            services.add(service);
        }

        if(aircond.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(aircond.getText().toString());
            services.add(service);
        }

        if(windscreen.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(windscreen.getText().toString());
            services.add(service);
        }

        if(interior.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(interior.getText().toString());
            services.add(service);
        }

        if(electrical.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(electrical.getText().toString());
            services.add(service);
        }

        if(brake.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(brake.getText().toString());
            services.add(service);
        }

        if(painting.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(painting.getText().toString());
            services.add(service);
        }

        if(accessory.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(accessory.getText().toString());
            services.add(service);
        }

        if(tire.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(tire.getText().toString());
            services.add(service);
        }

        if(other.isChecked())
        {
            Service service = new Service();
            service.setAvailable(true);
            service.setName(otherText.getText().toString());
            services.add(service);
        }

        for (Service service : services) {
            String key = mDatabase.child("Merchants").child(user.getUid()).child("Services").push().getKey();
            mDatabase.child("Merchants").child(user.getUid()).child("Services").child(key).setValue(service.getName());
        }

        if(isEditing.equals("true"))
        {
            Toast.makeText(ChooseServiceActivity.this, "Done Saving!",
                    Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }else{
            Toast.makeText(ChooseServiceActivity.this, "Sign up success! Proceed to login!",
                    Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
        }


    }
}
