package app.carrepair;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.time.MonthDay;
import java.util.ArrayList;
import java.util.Calendar;

import app.carrepair.Models.Booking;
import app.carrepair.Models.Merchant;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorkshopDetailActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    String merchantId;

    Merchant merchant;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.pic)
    TextView pic;

    @BindView(R.id.phone)
    TextView phone;

    @BindView(R.id.service)
    TextView service;

    @BindView(R.id.operation_hour)
    TextView operationHour;

    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.time)
    EditText time;

    @BindView(R.id.date)
    EditText date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshop_detail);
        ButterKnife.bind(this);

        showProgressDialog();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        Intent intent = getIntent();
        merchantId = intent.getStringExtra("id");

        mDatabase.child("Merchants").child(merchantId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        merchant = dataSnapshot.getValue(Merchant.class);

                        name.setText(merchant.getCompanyName());
                        phone.setText(merchant.getPhone().toString());
                        address.setText(merchant.getCompanyAddress());
                        pic.setText(merchant.getPersonInChargeName());

                        StringBuilder serviceProvided = new StringBuilder();
                        serviceProvided.append("\nService Provided:\n");

                        for(DataSnapshot serviceSnapshot: dataSnapshot.child("Services").getChildren()){
                            String service = serviceSnapshot.getValue(String.class);
                            serviceProvided.append(service + "\n");
                        }
                        service.setText(serviceProvided.toString());

                        ArrayList<String> hourArray = new ArrayList<>();

                        for (int i = 0; i <= 6; i++) {
                            hourArray.add("");
                        }

                        for(DataSnapshot hourSnapshot: dataSnapshot.child("OperationHours").getChildren()){
                            String time = hourSnapshot.getValue(String.class);

                            if(hourSnapshot.getKey().equals("monday")){
                                hourArray.set(0, "Monday: " + time);
                            }
                            if(hourSnapshot.getKey().equals("tuesday")){
                                hourArray.set(1, "Tuesday: " + time);
                            }
                            if(hourSnapshot.getKey().equals("wednesday")){
                                hourArray.set(2, "Wednesday: " + time);
                            }
                            if(hourSnapshot.getKey().equals("thursday")){
                                hourArray.set(3, "Thursday: " + time);
                            }
                            if(hourSnapshot.getKey().equals("friday")){
                                hourArray.set(4, "Friday: " + time);
                            }
                            if(hourSnapshot.getKey().equals("saturday")){
                                hourArray.set(5, "Saturday: " + time);
                            }
                            if(hourSnapshot.getKey().equals("sunday")){
                                hourArray.set(6, "Sunday: " + time);
                            }
                        }

                        StringBuilder operationHourString = new StringBuilder();
                        operationHourString.append("Operating Hour:\n");
                        for(String hour: hourArray){
                            operationHourString.append(hour + "\n");
                        }

                        operationHour.setText(operationHourString);

                        hideProgressDialog();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Firebase:", "error", databaseError.toException());
                    }
                });
    }

    @OnClick(R.id.book)
    public void onBookClicked(){
        showProgressDialog();

        String key = mDatabase.child("Bookings").push().getKey();
        Booking booking = new Booking(description.getText().toString(), date.getText().toString(), time.getText().toString(), user.getUid(), merchantId, "Pending");
        hideProgressDialog();

        mDatabase.child("Bookings").child(key).setValue(booking);

        String merchantBookingKey = mDatabase.child("Merchants").child(merchantId).child("Bookings").push().getKey();
        mDatabase.child("Merchants").child(merchantId).child("Bookings").child(merchantBookingKey).setValue(key);


        String clientBookingKey = mDatabase.child("Clients").child(user.getUid()).child("Bookings").push().getKey();
        mDatabase.child("Clients").child(user.getUid()).child("Bookings").child(clientBookingKey).setValue(key);

        Toast.makeText(WorkshopDetailActivity.this, "Booking Created! Please wait for confirmation from workshop.",
                Toast.LENGTH_LONG).show();

        finish();
    }

    @OnClick(R.id.date)
    public void onDatePickerClicked() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                WorkshopDetailActivity.this,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @OnClick(R.id.time)
    public void onTimePickerClicked() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                WorkshopDetailActivity.this,12,0,true);
        tpd.show(getFragmentManager(), "TimePickerDialog");
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        time.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date.setText(dayOfMonth + "-" + monthOfYear + "-" + year);
    }
}
