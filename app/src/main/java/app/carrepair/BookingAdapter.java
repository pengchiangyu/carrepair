package app.carrepair;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import app.carrepair.Models.Booking;
import app.carrepair.Models.Merchant;

public class BookingAdapter extends ArrayAdapter<Booking> {
    public BookingAdapter(Context context, ArrayList<Booking> bookings) {
        super(context, 0, bookings);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Get the data item for this position
        Booking booking = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.date_list, parent, false);
        }
        TextView date = view.findViewById(R.id.date);
        date.setText(booking.getDate() + " - " + booking.getStatus());

        return view;
    }
}