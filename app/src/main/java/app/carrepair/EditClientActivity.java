package app.carrepair;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import app.carrepair.Models.Client;
import app.carrepair.Models.Merchant;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditClientActivity extends BaseActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    @BindView(R.id.save_button)
    Button saveButton;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.phone)
    EditText phone;

    @BindView(R.id.carBrand)
    EditText carBrand;

    @BindView(R.id.carModel)
    EditText carModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_client);
        ButterKnife.bind(this);
        showProgressDialog();

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        mDatabase.child("Clients").child(user.getUid()).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Client client = dataSnapshot.getValue(Client.class);

                        if(client == null)
                        {
                            return;
                        }

                        name.setText(client.getName());
                        phone.setText(String.format("%d",client.getPhone()));
                        carBrand.setText(client.getCarBrand());
                        carModel.setText(client.getCarModel());
                        hideProgressDialog();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Firebase:", "getUser:onCancelled", databaseError.toException());
                    }
                });
    }

    @OnClick(R.id.save_button)
    public void onSaveButtonClicked(){
        showProgressDialog();

        if(!tryParseInt(phone.getText().toString()))
        {
            hideProgressDialog();
            Toast.makeText(EditClientActivity.this, "Input only number at phone field!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if(!validateFields(new EditText[]{phone, name, carBrand, carModel })){
            hideProgressDialog();
            Toast.makeText(EditClientActivity.this, "All the fields cannot be empty!!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        user = mAuth.getCurrentUser();
        Client client = new Client(
                name.getText().toString(),
                Integer.parseInt(phone.getText().toString()),
                carBrand.getText().toString(),
                carModel.getText().toString());

        mDatabase.child("Users").child(user.getUid()).child("Details").setValue(client);
        Toast.makeText(EditClientActivity.this, "Details saved!",
                Toast.LENGTH_LONG).show();
        finish();
    }

    private boolean validateFields(EditText[] fields){
        for (EditText currentField : fields) {
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
