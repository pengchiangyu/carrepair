package app.carrepair;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import app.carrepair.Models.Booking;
import app.carrepair.Models.Notification;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    @BindView(R.id.lv_notifications)
    ListView lvNotifications;

    private FirebaseListAdapter<Notification> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        showProgressDialog();

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        initiate();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public void initiate(){
        user = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Query query = mDatabase.child("Clients").child(user.getUid()).child("notifications");

        FirebaseListOptions<Notification> options = new FirebaseListOptions.Builder<Notification>()
                .setQuery(query, Notification.class)
                .setLayout(R.layout.notification_list)
                .build();

        adapter = new FirebaseListAdapter<Notification>(options) {
            @Override
            protected void populateView(View v, Notification model, int position) {
                String date = model.getDateTime();
                TextView textView = v.findViewById(R.id.date);
                textView.setText(date);

                String message = model.getMessage();
                TextView messageTextView = v.findViewById(R.id.message);
                messageTextView.setText(message);
                findViewById(R.id.empty_message).setVisibility(View.GONE);
            }
        };
        lvNotifications.setAdapter(adapter);
        hideProgressDialog();
    }
}