package app.carrepair;

/**
 * Common interface for chat messages, helps share code between RTDB and Firestore examples.
 */
public abstract class AbstractChat {

    public abstract String getName();

    public abstract String getMessage();

    public abstract String getUid();

    public abstract String getStatus();

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);

}
