package app.carrepair;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.carrepair.Models.Booking;
import app.carrepair.Models.Client;
import app.carrepair.Models.Merchant;
import app.carrepair.Models.Notification;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PendingServiceMerchantDetailActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    @BindView(R.id.date_time)
    TextView dateTime;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.phone)
    TextView phone;

    @BindView(R.id.car_brand)
    TextView carBrand;

    @BindView(R.id.car_model)
    TextView carModel;

    @BindView(R.id.statusSpinner)
    Spinner statusSpinner;

    String spinnerValue;
    String bookingId;

    Booking booking;
    Merchant merchant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_service_merchant_detail);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        Intent intent = getIntent();
        bookingId = intent.getStringExtra("id");

        mDatabase.child("Bookings").child(bookingId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        booking = dataSnapshot.getValue(Booking.class);

                        dateTime.setText("Date: " + booking.getDate() + " - Time: " + booking.getTime());
                        description.setText(booking.getDescription());
                        status.setText("Status: " + booking.getStatus());

                        getAndSetClientDetails(booking.getUserId());

                        mDatabase.child("Merchants").child(booking.getMerchantId()).addListenerForSingleValueEvent(
                                new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        merchant = dataSnapshot.getValue(Merchant.class);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.w("Firebase:", "error", databaseError.toException());
                                    }
                                });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Firebase:", "error", databaseError.toException());
                    }
                });

        addStatusSpinner();
    }

    @OnClick(R.id.chat)
    public void onChatClicked(){
        Intent intent = new Intent(getApplicationContext(),ChatActivity.class);
        intent.putExtra("bookingId", bookingId);
        startActivity(intent);
    }

    @OnClick(R.id.change_status)
    public void onUpdateStatusClicked(){
        showProgressDialog();
        mDatabase.child("Bookings").child(bookingId).child("status").setValue(spinnerValue);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        Notification notification = new Notification();
        notification.setDateTime(dateFormat.format(date));
        notification.setMessage("Booking for " + merchant.getCompanyName() + " at " + booking.getDateTime() + "\nThe status is changed to " + spinnerValue + "!");

        String notificationKey = mDatabase.child("Clients").child(booking.getUserId()).child("notifications").push().getKey();
        mDatabase.child("Clients").child(booking.getUserId()).child("notifications").child(notificationKey).setValue(notification);

        Toast.makeText(PendingServiceMerchantDetailActivity.this, "Status successfully updated to " + spinnerValue + "!",
                Toast.LENGTH_LONG).show();
        hideProgressDialog();
    }

    public void getAndSetClientDetails(String userId){
        mDatabase.child("Clients").child(userId).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Client client = dataSnapshot.getValue(Client.class);

                        name.setText(client.getName());
                        phone.setText(String.format("%d",client.getPhone()));
                        carModel.setText(client.getCarModel());
                        carBrand.setText(client.getCarBrand());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Firebase:", "error", databaseError.toException());
                    }
                });
    }

    public void addStatusSpinner() {

        statusSpinner = (Spinner) findViewById(R.id.statusSpinner);
        List<String> list = new ArrayList<String>();
        list.add("Pending");
        list.add("Rejected");
        list.add("Processing");
        list.add("Done");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinner.setAdapter(dataAdapter);

        statusSpinner.setOnItemSelectedListener(new customOnItemSelected());
    }

    public class customOnItemSelected implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            spinnerValue = parent.getItemAtPosition(pos).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }

    }
}
