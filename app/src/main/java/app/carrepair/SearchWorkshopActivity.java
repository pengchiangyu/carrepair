package app.carrepair;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import app.carrepair.Models.Merchant;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchWorkshopActivity extends BaseActivity {
    private static final int PLACE_PICKER_REQUEST = 1;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    private FirebaseListAdapter<Merchant> adapter;

    ArrayList<Merchant> merchants;

    @BindView(R.id.lv_merchants)
    ListView lvMerchants;

    private Place place;
    Double latitude;
    Double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchworkshop);
        ButterKnife.bind(this);
        showProgressDialog();

        merchants = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        getAllMerchants();

        lvMerchants.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {

                Merchant merchant = (Merchant) lvMerchants.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(),WorkshopDetailActivity.class);
                intent.putExtra("id", merchant.getFirebaseKey());
                startActivity(intent);
            }
        });
    }

    public void setAdapter(){
        MerchantAdapter adapter =
                new MerchantAdapter(this, merchants);
        lvMerchants.setAdapter(adapter);
        hideProgressDialog();
    }

    public void getAllMerchants(){
        mDatabase.child("Merchants").addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot child: dataSnapshot.getChildren())
                        {
                            Merchant merchant = child.getValue(Merchant.class);
                            merchant.setFirebaseKey(child.getKey());
                            merchants.add(merchant);
                        }
                        setAdapter();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Firebase:", "error", databaseError.toException());
                    }
                });
    }

    @OnClick(R.id.location)
    public void onSelectLocation(){
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            showProgressDialog();
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                place = PlacePicker.getPlace(data, this);
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;

                sortWithLocation();
            }else{
                Toast.makeText(SearchWorkshopActivity.this, "Error! please contact administrator.",
                        Toast.LENGTH_LONG).show();
                hideProgressDialog();
            }
        }
        else{
            Log.w("Activity Result:", "Error");
        }
    }

    public void sortWithLocation(){
        for(Merchant merchant: merchants)
        {
            String origin = latitude + "," + longitude;
            String destination = merchant.getLatitude() + "," + merchant.getLongitude();
            String apiUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin + "&destinations=" + destination +"&key=AIzaSyDhMZrfuTL7FwlASIoPxFP9-y2O9HNg3FU";
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            HttpURLConnection urlConnection = null;

            URL url = null;
            try {
                url = new URL(apiUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setReadTimeout(10000 /* milliseconds */ );
                urlConnection.setConnectTimeout(15000 /* milliseconds */ );
                urlConnection.setDoOutput(true);
                urlConnection.connect();

                BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
                StringBuilder sb = new StringBuilder();

                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //sb.toString() is the JSON Result returned
                JSONObject jsonObject = new JSONObject(sb.toString());
                JSONArray rows = (JSONArray) jsonObject.get("rows");

                for (int i = 0; i < rows.length(); i++) {
                    JSONObject object = (JSONObject) rows.get(i);
                    JSONArray elements = (JSONArray) object.get("elements");
                    for (int j = 0; j < elements.length(); j++) {
                        JSONObject object2 = (JSONObject) elements.get(i);
                        JSONObject distance = (JSONObject) object2.get("distance");
                        String value = distance.getString("value");
                        merchant.setDistance(Integer.parseInt(value));
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for(int i=0; i < merchants.size(); i++){
            for(int j=1; j < (merchants.size()-i); j++){
                if(merchants.get(j-1).getDistance() > merchants.get(j).getDistance()){
                    Collections.swap(merchants, j, j-1);
                }
            }
        }

        setAdapter();
    }
}
