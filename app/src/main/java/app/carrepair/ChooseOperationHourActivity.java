package app.carrepair;

import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Iterator;

import app.carrepair.Models.Day;
import app.carrepair.Models.Days;
import app.carrepair.Models.Time;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseOperationHourActivity extends BaseActivity
        implements TimePickerDialog.OnTimeSetListener{

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    @BindView(R.id.monday)
    CheckBox mondayCheckbox;

    @BindView(R.id.tuesday)
    CheckBox tuesdayCheckbox;

    @BindView(R.id.wednesday)
    CheckBox wednesdayCheckbox;

    @BindView(R.id.thursday)
    CheckBox thursdayCheckbox;

    @BindView(R.id.friday)
    CheckBox fridayCheckbox;

    @BindView(R.id.saturday)
    CheckBox saturdayCheckbox;

    @BindView(R.id.sunday)
    CheckBox sundayCheckbox;

    Day monday = new Day();
    Day tuesday = new Day();
    Day wednesday = new Day();
    Day thursday = new Day();
    Day friday = new Day();
    Day saturday = new Day();
    Day sunday = new Day();

    EditText currentEditText;

    String currentDay;
    String isEditing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operationhour);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        Intent intent = getIntent();
        isEditing = intent.getStringExtra("isEditing");
        if(isEditing == null)
        {
            isEditing  = "false";
        }
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        EditText text;
        switch(currentDay){
            case "monday-open":
                monday.setOpenTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.monday_startHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "monday-close":
                monday.setCloseTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.monday_endHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "tuesday-open":
                tuesday.setOpenTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.tuesday_startHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "tuesday-close":
                tuesday.setCloseTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.tuesday_endHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "wednesday-open":
                wednesday.setOpenTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.wednesday_startHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "wednesday-close":
                wednesday.setCloseTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.wednesday_endHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "thursday-open":
                thursday.setOpenTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.thursday_startHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "thursday-close":
                thursday.setCloseTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.thursday_endHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "friday-open":
                friday.setOpenTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.friday_startHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "friday-close":
                friday.setCloseTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.friday_endHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "saturday-open":
                saturday.setOpenTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.saturday_startHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "saturday-close":
                saturday.setCloseTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.saturday_endHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "sunday-open":
                sunday.setOpenTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.sunday_startHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            case "sunday-close":
                sunday.setCloseTime(new Time(hourOfDay, minute));
                text =  findViewById(R.id.sunday_endHour);
                text.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.monday)
    public void onMondayClicked(){
        if(!mondayCheckbox.isChecked())
        {
            findViewById(R.id.monday_hour).setVisibility(View.GONE);
            monday = new Day();
        }else{
            monday.setOn(true);
            findViewById(R.id.monday_hour).setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.monday_startHour)
    public void onMondayStartHourClicked(){
        int hour = 9;
        int minute = 0;

        if(monday != null && monday.getOpenTime() != null)
        {
            hour = monday.getOpenTime().getHour();
            minute = monday.getOpenTime().getMinute();
        }

        currentDay = "monday-open";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.monday_endHour)
    public void onMondayEndHourClicked(){
        int hour = 18;
        int minute = 0;

        if(monday != null && monday.getCloseTime() != null)
        {
            hour = monday.getCloseTime().getHour();
            minute = monday.getCloseTime().getMinute();
        }

        currentDay = "monday-close";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.tuesday)
    public void onTuesdayClicked(){
        if(!tuesdayCheckbox.isChecked())
        {
            findViewById(R.id.tuesday_hour).setVisibility(View.GONE);
            tuesday = new Day();
        }else{
            tuesday.setOn(true);
            findViewById(R.id.tuesday_hour).setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.tuesday_startHour)
    public void onTuesdayStartHourClicked(){
        int hour = 9;
        int minute = 0;

        if(tuesday != null && tuesday.getOpenTime() != null)
        {
            hour = tuesday.getOpenTime().getHour();
            minute = tuesday.getOpenTime().getMinute();
        }

        currentDay = "tuesday-open";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.tuesday_endHour)
    public void onTuesdayEndHourClicked(){
        int hour = 18;
        int minute = 0;

        if(tuesday != null && tuesday.getCloseTime() != null)
        {
            hour = tuesday.getCloseTime().getHour();
            minute = tuesday.getCloseTime().getMinute();
        }

        currentDay = "tuesday-close";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.wednesday)
    public void onWednesdayClicked(){
        if(!wednesdayCheckbox.isChecked())
        {
            findViewById(R.id.wednesday_hour).setVisibility(View.GONE);
            wednesday = new Day();
        }else{
            wednesday.setOn(true);
            findViewById(R.id.wednesday_hour).setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.wednesday_startHour)
    public void onWednesdayStartHourClicked(){
        int hour = 9;
        int minute = 0;

        if(wednesday != null && wednesday.getOpenTime() != null)
        {
            hour = wednesday.getOpenTime().getHour();
            minute = wednesday.getOpenTime().getMinute();
        }

        currentDay = "wednesday-open";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.wednesday_endHour)
    public void onWednesdayEndHourClicked(){
        int hour = 18;
        int minute = 0;

        if(wednesday != null && wednesday.getCloseTime() != null)
        {
            hour = wednesday.getCloseTime().getHour();
            minute = wednesday.getCloseTime().getMinute();
        }

        currentDay = "wednesday-close";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.thursday)
    public void onThursdayClicked(){
        if(!thursdayCheckbox.isChecked())
        {
            findViewById(R.id.thursday_hour).setVisibility(View.GONE);
            thursday = new Day();
        }else{
            thursday.setOn(true);
            findViewById(R.id.thursday_hour).setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.thursday_startHour)
    public void onThursdayStartHourClicked(){
        int hour = 9;
        int minute = 0;

        if(thursday != null && thursday.getOpenTime() != null)
        {
            hour = thursday.getOpenTime().getHour();
            minute = thursday.getOpenTime().getMinute();
        }

        currentDay = "thursday-open";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.thursday_endHour)
    public void onThursdayEndHourClicked(){
        int hour = 18;
        int minute = 0;

        if(thursday != null && thursday.getCloseTime() != null)
        {
            hour = thursday.getCloseTime().getHour();
            minute = thursday.getCloseTime().getMinute();
        }

        currentDay = "thursday-close";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.friday)
    public void onFridayClicked(){
        if(!fridayCheckbox.isChecked())
        {
            findViewById(R.id.friday_hour).setVisibility(View.GONE);
            friday = new Day();
        }else{
            friday.setOn(true);
            findViewById(R.id.friday_hour).setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.friday_startHour)
    public void onFridayStartHourClicked(){
        int hour = 9;
        int minute = 0;

        if(friday != null && friday.getOpenTime() != null)
        {
            hour = friday.getOpenTime().getHour();
            minute = friday.getOpenTime().getMinute();
        }

        currentDay = "friday-open";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.friday_endHour)
    public void onFridayEndHourClicked(){
        int hour = 18;
        int minute = 0;

        if(friday != null && friday.getCloseTime() != null)
        {
            hour = friday.getCloseTime().getHour();
            minute = friday.getCloseTime().getMinute();
        }

        currentDay = "friday-close";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.saturday)
    public void onSaturdayClicked(){
        if(!saturdayCheckbox.isChecked())
        {
            findViewById(R.id.saturday_hour).setVisibility(View.GONE);
            saturday = new Day();
        }else{
            saturday.setOn(true);
            findViewById(R.id.saturday_hour).setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.saturday_startHour)
    public void onSaturdayStartHourClicked(){
        int hour = 9;
        int minute = 0;

        if(saturday != null && saturday.getOpenTime() != null)
        {
            hour = saturday.getOpenTime().getHour();
            minute = saturday.getOpenTime().getMinute();
        }

        currentDay = "saturday-open";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.saturday_endHour)
    public void onSaturdayEndHourClicked(){
        int hour = 18;
        int minute = 0;

        if(saturday != null && saturday.getCloseTime() != null)
        {
            hour = saturday.getCloseTime().getHour();
            minute = saturday.getCloseTime().getMinute();
        }

        currentDay = "saturday-close";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.sunday)
    public void onSundayClicked(){
        if(!sundayCheckbox.isChecked())
        {
            findViewById(R.id.sunday_hour).setVisibility(View.GONE);
            sunday = new Day();
        }else{
            sunday.setOn(true);
            findViewById(R.id.sunday_hour).setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.sunday_startHour)
    public void onSundayStartHourClicked(){
        int hour = 9;
        int minute = 0;

        if(sunday != null && sunday.getOpenTime() != null)
        {
            hour = sunday.getOpenTime().getHour();
            minute = sunday.getOpenTime().getMinute();
        }

        currentDay = "sunday-open";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.sunday_endHour)
    public void onSundayEndHourClicked(){
        int hour = 18;
        int minute = 0;

        if(sunday != null && sunday.getCloseTime() != null)
        {
            hour = sunday.getCloseTime().getHour();
            minute = sunday.getCloseTime().getMinute();
        }

        currentDay = "sunday-close";
        // Create a new instance of Time    PickerDialog and return it
        TimePickerDialog timepicker = new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this));
        timepicker.show();
    }

    @OnClick(R.id.operation_next_button)
    public void onNextButonClicked(){
        Days days = new Days(
                monday,
                tuesday,
                wednesday,
                thursday,
                friday,
                saturday,
                sunday
        );

        if(monday.getOn())
        {
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("monday").setValue(monday.getOperationHour());
        }else{
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("monday").setValue("Closed");
        }

        if(tuesday.getOn())
        {
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("tuesday").setValue(tuesday.getOperationHour());
        }else{
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("tuesday").setValue("Closed");
        }

        if(wednesday.getOn())
        {
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("wednesday").setValue(wednesday.getOperationHour());
        }else{
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("wednesday").setValue("Closed");
        }

        if(thursday.getOn())
        {
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("thursday").setValue(thursday.getOperationHour());
        }else{
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("thursday").setValue("Closed");
        }

        if(friday.getOn())
        {
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("friday").setValue(friday.getOperationHour());
        }else{
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("friday").setValue("Closed");
        }

        if(saturday.getOn())
        {
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("saturday").setValue(saturday.getOperationHour());
        }else{
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("saturday").setValue("Closed");
        }

        if(sunday.getOn())
        {
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("sunday").setValue(sunday.getOperationHour());
        }else{
            mDatabase.child("Merchants").child(user.getUid()).child("OperationHours").child("sunday").setValue("Closed");
        }

        if(isEditing.equals("true"))
        {
            Intent intent = new Intent(getApplicationContext(),ChooseServiceActivity.class);
            intent.putExtra("isEditing", "true");
            startActivity(intent);
        }else{
            startActivity(new Intent(getApplicationContext(),ChooseServiceActivity.class));
        }
    }
}
