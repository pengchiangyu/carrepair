package app.carrepair;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import app.carrepair.Models.Merchant;

public class MerchantAdapter extends ArrayAdapter<Merchant> {
    public MerchantAdapter(Context context, ArrayList<Merchant> merchants) {
        super(context, 0, merchants);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Get the data item for this position
        Merchant merchant = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.category_list, parent, false);
        }
        TextView title = view.findViewById(R.id.title);
                title.setText(merchant.getCompanyName());

                TextView address = view.findViewById(R.id.address);
                address.setText(merchant.getCompanyAddress());

                if(merchant.getImageUrl() != null)
                {
                    ImageView image = view.findViewById(R.id.icon);
                    byte[] decodedString = Base64.decode(merchant.getImageUrl(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    image.setImageBitmap(decodedByte);
                }

        return view;
    }
}