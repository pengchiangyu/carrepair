package app.carrepair;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import app.carrepair.Models.Booking;
import app.carrepair.Models.Merchant;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PendingServiceDetailActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser user;

    String bookingId;

    @BindView(R.id.date_time)
    TextView dateTime;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.status)
    TextView status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_service_detail);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        Intent intent = getIntent();
        bookingId = intent.getStringExtra("id");

        mDatabase.child("Bookings").child(bookingId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Booking booking = dataSnapshot.getValue(Booking.class);

                        dateTime.setText("Date: " + booking.getDate() + " - Time: " + booking.getTime());
                        description.setText(booking.getDescription());
                        status.setText("Status: " + booking.getStatus());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Firebase:", "error", databaseError.toException());
                    }
                });
    }

    @OnClick(R.id.chat)
    public void onChatClicked(){
        Intent intent = new Intent(getApplicationContext(),ChatActivity.class);
        intent.putExtra("bookingId", bookingId);
        startActivity(intent);
    }
}
